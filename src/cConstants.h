#ifndef CCPPCONSTANTS_H_INCLUDED
#define CCPPCONSTANTS_H_INCLUDED

static const char* FILE_TO_LOAD = "data/StationWebcamsDL.xml";
static const char* FAVS_TO_LOAD = "data/StationWebcamsFavs.xml";
static const char* FILE_VAL_DATE = "data/Validationdate.xml";
static const char* URL_VAL_DATE = "http://www.twistit.fr/data/Validationdate.xml";
static const char* FILE_URL = "http://www.twistit.fr/data/StationsFinal35.xml";

#endif // CCPPCONSTANTS_H_INCLUDED
