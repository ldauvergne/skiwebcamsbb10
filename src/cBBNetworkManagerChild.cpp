/*
 * cBBNetworkManagerChild.cpp
 *
 *  Created on: 10 déc. 2013
 *      Author: Pipould
 */

#include "cBBNetworkManagerChild.hpp"
#include "cNetworkManager.hpp"

cBBNetworkManagerChild::cBBNetworkManagerChild(QObject *parent)
{
    aParent = parent;
    pProgToast = new SystemProgressToast();
}

void cBBNetworkManagerChild::showToast(QString msg, QString btnMsg, bool showButton)
{
    // Display the message on screen.
    SystemToast* toast = new bb::system::SystemToast(this);
    SystemUiButton* toastRetryBtn = NULL;

    if (showButton) {
        toastRetryBtn = toast->button();
    }

    bool result;
    Q_UNUSED(result);

    result = connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
            SLOT(onToastFinished(bb::system::SystemUiResult::Type)));

    Q_ASSERT(result);

    if (showButton) {
        toastRetryBtn->setLabel(btnMsg);
    }
    toast->setBody(msg);
    toast->setPosition(bb::system::SystemUiPosition::MiddleCenter);
    toast->show();
}

// Gives the user 3 chances to re-establish the network connection before the
// app asks them to exit.
void cBBNetworkManagerChild::onToastFinished(bb::system::SystemUiResult::Type resultType)
{
    if (((cNetworkManager*)aParent)->numOfRetries != 3 && resultType == SystemUiResult::ButtonSelection) {
        ((cNetworkManager*)aParent)->numOfRetries += 1;
        ((cNetworkManager*)aParent)->RequestFileFromNetwork(((cNetworkManager*)aParent)->aFile,((cNetworkManager*)aParent)->aUrl);
    } else if (((cNetworkManager*)aParent)->networkConnected) {
        ((cNetworkManager*)aParent)->numOfRetries = 1;
    } else {
        showFinalDialog();
    }
}

void cBBNetworkManagerChild::onFinalDialogFinished(bb::system::SystemUiResult::Type resultType)
{
    if (resultType == SystemUiResult::ConfirmButtonSelection) {
        QCoreApplication::quit();
    } else {
        ((cNetworkManager*) aParent)->numOfRetries = 1;
        ((cNetworkManager*) aParent)->RequestFileFromNetwork(((cNetworkManager*) aParent)->aFile,
                ((cNetworkManager*) aParent)->aUrl);
    }
}

// Shows the toast with the an exit message.
void cBBNetworkManagerChild::showFinalDialog()
{
    SystemDialog* dialog = new bb::system::SystemDialog(this);
    SystemUiButton* toastCancelBtn = dialog->cancelButton();
    SystemUiButton* toastExitBtn = dialog->confirmButton();

    toastCancelBtn->setLabel("Cancel");
    toastExitBtn->setLabel("Exit app");
    dialog->setBody("A network connection could not be re-established");
    dialog->show();

    bool result;
    Q_UNUSED(result);

    result = connect(dialog, SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
            SLOT(onFinalDialogFinished(bb::system::SystemUiResult::Type)));

    Q_ASSERT(result);
}

cBBNetworkManagerChild::~cBBNetworkManagerChild()
{

}

