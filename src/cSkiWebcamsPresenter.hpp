/*
 * cSkiWebcamsPresenter.h
 *
 *  Created on: 7 déc. 2013
 *      Author: Pipould
 */

#ifndef CSKIWEBCAMSPRESENTER_H_
#define CSKIWEBCAMSPRESENTER_H_
#include "skiwebcamsdatahandler/cSkiWebcamsDataHandler.h"
#include "cConstants.h"

#include <bb/cascades/GroupDataModel>

#include <QtCore/QObject>
#include <QDir>
#include <unistd.h>
#include <list>

class cSkiWebcamsPresenter: public QObject
{
Q_OBJECT
    // The model that provides the filtered list of accounts
    Q_PROPERTY(bb::cascades::GroupDataModel *countries_model READ countries_model CONSTANT)
    ;Q_PROPERTY(bb::cascades::GroupDataModel *resorts_model READ resorts_model CONSTANT)
    ;Q_PROPERTY(bb::cascades::GroupDataModel *webcams_url_model READ webcams_url_model CONSTANT)
    ;Q_PROPERTY(bb::cascades::GroupDataModel *favorites_model READ favorites_model CONSTANT)
    ;Q_PROPERTY(QString aCountriesViewMessage READ getCountriesViewMessage NOTIFY sCountryMessageChanged)
    ;Q_PROPERTY(bool aHasFavourites READ mHasFavourites NOTIFY sHasFavouritesChanged)
    ;Q_PROPERTY(QString aDataDate READ mGetDataDate NOTIFY sDataDateChanged)
    ;
public:
    cSkiWebcamsPresenter(QObject *parent = 0);Q_INVOKABLE
    void mInitPresenter();

    void mLoadAll();Q_INVOKABLE
    void mGetCountriesName();Q_INVOKABLE
    void mGetResortsName(int pCountryIndex);Q_INVOKABLE
    void mGetWebcams(int pResortIndex);Q_INVOKABLE
    void mGetWebcams(int pCountryIndex,int pResortIndex);Q_INVOKABLE
    void mGetFavoriteWebcams(int pIndex);Q_INVOKABLE

    void mGetFavoritesName();Q_INVOKABLE
    void mAddFavorite(int pResortIndex);Q_INVOKABLE
    void mDeleteFavorite(int pIndex);Q_INVOKABLE

    void mSetSelectedCountry(int pCountryIndex);Q_INVOKABLE
    void mSetCountriesViewMessage(QString* pMessage);
    void mSetDataDate();
    QString mGetDataDate();Q_INVOKABLE
    bool mHasFavourites();

    virtual ~cSkiWebcamsPresenter();
signals:
    void sCountryMessageChanged();
    void sHasFavouritesChanged();
    void sDataDateChanged();
private:
    int aSelectedCountry;

    cSkiWebcamsDataHandler *aDataHandler;

    bool aHasFavourites;
    QString* aCountriesViewMessage;
    QString* aDataDate;

    QTextCodec *aCodec;

    QString getCountriesViewMessage();

    // The property values
    bb::cascades::GroupDataModel* countries_names;
    bb::cascades::GroupDataModel* resorts_names;
    bb::cascades::GroupDataModel* webcams_url;
    bb::cascades::GroupDataModel* favorites_names;

    // The accessor methods of the properties
    bb::cascades::GroupDataModel* countries_model() const;
    bb::cascades::GroupDataModel* resorts_model() const;
    bb::cascades::GroupDataModel* webcams_url_model() const;
    bb::cascades::GroupDataModel* favorites_model() const;

};

#endif /* CSKIWEBCAMSPRESENTER_H_ */
