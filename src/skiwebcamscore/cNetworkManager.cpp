/*
 * cNetworkManagement.cpp
 *
 *  Created on: 4 déc. 2013
 *      Author: Pipould
 */

#include "cNetworkManager.hpp"


cNetworkManager::cNetworkManager(QObject *pParent)

{
    // Initialize member variables.
    pNetworkAccessManager = new QNetworkAccessManager(this);
    networkConnected = false;
    numOfRetries = 1;

    reply = NULL;
    aFile=NULL;
    aUrl=NULL;
    aParent=pParent;
    aBBNetManagerChild = new cBBNetworkManagerChild(this);
    // Create a file in the device file system that
    // we can use to save the data model.


    // Create a status event object that is used to listen for changes
    // in the network status and/or connectivity interface.
    // When a change signal is received, it'll trigger the
    // onNetworkStatusUpdated slot to update the status for us.
    statusEvent = new StatusEvent();

    // Check signal and slot connections.
    bool result;
    Q_UNUSED(result);

    result = connect(statusEvent, SIGNAL(networkStatusUpdated(bool,QString)), this,
            SLOT(onNetworkStatusUpdated(bool,QString)));

    Q_ASSERT(result);

    result = connect(pNetworkAccessManager, SIGNAL(finished(QNetworkReply*)), this,
            SLOT(onRequestFinished(QNetworkReply*)));

    Q_ASSERT(result);

    result = connect(this, SIGNAL(processReply(QNetworkReply*)), this,
            SLOT(onProcessReply(QNetworkReply*)));

    Q_ASSERT(result);

    qDebug() << "cNetworkManagement";
}

// Updates the status of the network connection in the UI.
void cNetworkManager::onNetworkStatusUpdated(bool connectionStatus, QString interfaceType)
{
    qDebug() << "Entering onNetworkStatusUpdated";
    networkConnected = connectionStatus;
    if (networkConnected) {
        emit networkavailable();
    }
}


void cNetworkManager::onRequestFinished(QNetworkReply* reply)
{
    qDebug() << "Entering onRequestFinished";
    QNetworkReply::NetworkError netError = reply->error();

    if (netError == QNetworkReply::NoError) {
        // Emit updateListView signal to set the
        // new data model on the list.
        qDebug() << "No errors onRequestFinished";
        emit processReply(reply);
    } else {
        qDebug() << "netError";
       emit networkErrorOccured(reply->errorString());
    }
}

void cNetworkManager::onProcessReply(QNetworkReply* reply)
{
    qDebug() << "Entering onProcessReply";

    QByteArray lReply = reply->readAll();

    QString *lStringReply = new QString("");
    lStringReply->append(lReply);

    qDebug() << lStringReply;

    // Open the file.
    if (aFile->open(QIODevice::ReadWrite) == false) {
        qDebug() << "Failed to open the file (onProcessReply)";
        numOfRetries = 3;
        aBBNetManagerChild->showToast("Failed to open the file", "Exit app", true);
    } else {
        qDebug() << "Succeed to open file (onProcessReply)";
        // Write to the file using the reply data
        // and close the file.
        aFile->write(lStringReply->toUtf8());
        aFile->flush();
        aFile->close();
        qDebug() << "Write to file ok, leaving onProcessReply";
        emit updateFileFinished();
    }
    reply->deleteLater();
}

void cNetworkManager::RequestFileFromNetwork(QFile* pFileToWrite,QUrl* pUrl)
{
    if (networkConnected) {

        aFile=pFileToWrite;
        qDebug() << "Entering onRequestFileFromNetwork";
        // Create and send the network request.
        QNetworkRequest request = QNetworkRequest();
        aUrl=pUrl;
        request.setUrl(*aUrl);
        reply = pNetworkAccessManager->get(request);
        // Show download progress.
        bool result;
        Q_UNUSED(result);
        result = connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this,
                SLOT(onDownloadProgress(qint64, qint64)),Qt::UniqueConnection);
        Q_ASSERT(result);
    }
    else {
            aBBNetManagerChild->showToast("No network connection is active", "Retry?", true);
        }
}

void cNetworkManager::onDownloadProgress(qint64 bytesSent, qint64 bytesTotal)
{
    if (bytesSent == 0 || bytesTotal == 0)
        return;

    int currentProgress = (bytesSent * 100) / bytesTotal;
    aBBNetManagerChild->pProgToast->setBody("Contacting network to download file ...");
    aBBNetManagerChild->pProgToast->setProgress(currentProgress);
    aBBNetManagerChild->pProgToast->setState(SystemUiProgressState::Active);
    aBBNetManagerChild->pProgToast->setPosition(SystemUiPosition::MiddleCenter);
    aBBNetManagerChild->pProgToast->show();
}








