/*
 * cSkiWebcamsCore.cpp
 *
 *  Created on: 29 nov. 2013
 *      Author: Pipould
 */

#include "cSkiWebcamsCore.hpp"

cSkiWebcamsCore::cSkiWebcamsCore(QObject *parent) :
        QObject(parent)
{
    aRetry=true;
    aAppStarted = false;
    aNetworkManager = new cNetworkManager(this);
    aSkiWebcamsPresenter = new cSkiWebcamsPresenter(this);

    bool result;
    Q_UNUSED(result);

    result = connect(aNetworkManager, SIGNAL(networkavailable()), this, SLOT(onNetworkAvailable()));

    Q_ASSERT(result);

    result = connect(aNetworkManager, SIGNAL(networkErrorOccured(QString)), this, SLOT(onNetworkErrorOccured(QString)));

    Q_ASSERT(result);


}

cNetworkManager* cSkiWebcamsCore::mGetNetworkManager()
{
    return aNetworkManager;
}


cSkiWebcamsPresenter* cSkiWebcamsCore::mGetPresenter()
{
    return aSkiWebcamsPresenter;
}

void cSkiWebcamsCore::onNetworkAvailable()
{
    qDebug() << "";
    qDebug() << "onNetworkAvailable Start";
    disconnect(this, SLOT(onNetworkAvailable()));

    if (!aAppStarted) {
        aAppStarted = true;

        bool result;
        Q_UNUSED(result);

        result = connect(aNetworkManager, SIGNAL(updateFileFinished()), this,
                SLOT(onGetDateFileFinished()),Qt::UniqueConnection);

        Q_ASSERT(result);

        aNetworkManager->RequestFileFromNetwork(new QFile(QString(FILE_VAL_DATE)), new QUrl(QString(URL_VAL_DATE)));

    }
    qDebug() << "onNetworkAvailable End";
}

void cSkiWebcamsCore::onGetDateFileFinished()
{
    qDebug() << "";
    qDebug() << "onGetDateFileFinished Start";

    bool result;

    Q_UNUSED(result);

    result = disconnect(aNetworkManager,SIGNAL(updateFileFinished()),this, SLOT(onGetDateFileFinished()));

    Q_ASSERT(result);

    QFile fileDate(FILE_VAL_DATE);

    if (!fileDate.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Failed " << endl;
        return;
    }

    while (!fileDate.atEnd()) {
        QByteArray line = fileDate.readLine();
        fileDateString = QString(line);
        fileDateString.resize(24);
    }

    qDebug() << "onGetDateFileFinished Date: " << fileDateString << endl;

    QFile fileData(FILE_TO_LOAD);
    if (!fileData.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "onGetDateFileFinished File NOT existing";
        mGetWebcamsFile();
    } else {
        qDebug() << "onGetDateFileFinished File exists";
        onUpdateFileFinished();
    }
    qDebug() << "onGetDateFileFinished End";
}

void cSkiWebcamsCore::onNetworkErrorOccured(QString netError)
{
    qDebug() << "";
    qDebug() << "onNetworkErrorOccured Start";

    aSkiWebcamsPresenter->mSetCountriesViewMessage((new QString("Error -" + netError.split('-').last())));

    qDebug() << "onNetworkErrorOccured End";
}

void cSkiWebcamsCore::onUpdateFileFinished()
{
    qDebug() << "";
    qDebug() << "onUpdateFileFinished Start";

    disconnect(aNetworkManager,SIGNAL(updateFileFinished()),this, SLOT(onUpdateFileFinished()));

    aSkiWebcamsPresenter->mInitPresenter();

    qDebug() << "onUpdateFileFinished File Date: " << fileDateString << endl;
    qDebug() << "onUpdateFileFinished Presenter Date: " << aSkiWebcamsPresenter->mGetDataDate() << endl;

    if ((aSkiWebcamsPresenter->mGetDataDate().compare(fileDateString) != 0) && aRetry) {
        mGetWebcamsFile();
        aRetry=false;
    } else {
        qDebug() << "onUpdateFileFinished Same Dates";
        aSkiWebcamsPresenter->mLoadAll();
        aSkiWebcamsPresenter->mGetCountriesName();
        aSkiWebcamsPresenter->mGetFavoritesName();
    }
    qDebug() << "onUpdateFileFinished End";
}

void cSkiWebcamsCore::mGetWebcamsFile()
{
    bool result;

    Q_UNUSED(result);

    result = connect(aNetworkManager, SIGNAL(updateFileFinished()), this,
            SLOT(onUpdateFileFinished()),Qt::UniqueConnection);

    Q_ASSERT(result);

    aNetworkManager->RequestFileFromNetwork(new QFile(QString(FILE_TO_LOAD)), new QUrl(QString(FILE_URL)));
}

cSkiWebcamsCore::~cSkiWebcamsCore()
{
    // TODO Auto-generated destructor stub
}

