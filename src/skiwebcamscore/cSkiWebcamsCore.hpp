/*
 * cSkiWebcamsCore.h
 *
 *  Created on: 29 nov. 2013
 *      Author: Pipould
 */

#ifndef CSKIWEBCAMSCORE_H_
#define CSKIWEBCAMSCORE_H_

#include "cNetworkManager.hpp"
#include "../cSkiWebcamsPresenter.hpp"

#include <QtCore/QObject>
#include <QVariant>

class cSkiWebcamsCore: public QObject
{
Q_OBJECT

public:
    cSkiWebcamsCore(QObject *parent = 0);Q_INVOKABLE
    bool aAppStarted;
    cSkiWebcamsPresenter* mGetPresenter();
    cNetworkManager* mGetNetworkManager();

    virtual ~cSkiWebcamsCore();

private slots:
    void onGetDateFileFinished();
    void onUpdateFileFinished();
    void onNetworkAvailable();
    void onNetworkErrorOccured(QString);

private:
    bool aRetry;
    QString fileDateString;
    cNetworkManager *aNetworkManager;
    cSkiWebcamsPresenter *aSkiWebcamsPresenter;
    void mGetWebcamsFile();

};

#endif /* CSKIWEBCAMSCORE_H_ */
