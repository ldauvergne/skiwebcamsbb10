/*
 * cNetworkManagement.h
 *
 *  Created on: 4 déc. 2013
 *      Author: Pipould
 */

#ifndef CNETWORKMANAGEMENT_H_
#define CNETWORKMANAGEMENT_H_

#include "../StatusEvent.h"
#include "../cConstants.h"
#include "../cBBNetworkManagerChild.hpp"

#include <QObject>
#include <QString>
#include <QFile>
#include <QtNetwork>

class cNetworkManager: public QObject
{
Q_OBJECT

public:
    cNetworkManager(QObject *parent = 0);
    int numOfRetries;
    bool networkConnected;
    QFile *aFile;
    QUrl *aUrl;

    virtual ~cNetworkManager()
    {
    }

    void RequestFileFromNetwork(QFile*,QUrl*);

signals:
    void networkStatusUpdated(bool, QString);
    void processReply(QNetworkReply*);
    void updateFileFinished();
    void networkavailable();
    void networkErrorOccured(QString);

private slots:

    void onNetworkStatusUpdated(bool, QString);
    void onProcessReply(QNetworkReply*);
    void onRequestFinished(QNetworkReply*);
    void onDownloadProgress(qint64, qint64);

private:
    QObject *aParent;
    cBBNetworkManagerChild* aBBNetManagerChild;
    QNetworkAccessManager* pNetworkAccessManager;
    QNetworkReply* reply;
    StatusEvent* statusEvent;

    // If the network connection is lost, these
    // toasts and flags are used to help the
    // app re-establish the user's network
    // connection, or exit the app.

};

#endif /* CNETWORKMANAGEMENT_H_ */
