/*
 * cBBNetworkManagerChild.h
 *
 *  Created on: 10 déc. 2013
 *      Author: Pipould
 */

#ifndef CBBNETWORKMANAGERCHILD_H_
#define CBBNETWORKMANAGERCHILD_H_

#include <bps/netstatus.h>
#include <bb/AbstractBpsEventHandler>
#include <bb/system/SystemProgressToast>
#include <bb/AbstractBpsEventHandler>
#include <bb/system/SystemToast>
#include <bb/system/SystemDialog>
#include <bps/bps.h>
#include <bps/netstatus.h>
#include <bps/locale.h>

namespace bb
{
    namespace cascades
    {
        class Application;
        class LocaleHandler;
    }
}

using namespace bb::cascades;
using namespace bb::system;

class cBBNetworkManagerChild: public QObject
{
Q_OBJECT
public:
    cBBNetworkManagerChild(QObject* parent);
    void showToast(QString, QString, bool);
    QObject *aParent;
    SystemProgressToast* pProgToast;
    private slots:
    void onToastFinished(bb::system::SystemUiResult::Type);
    void onFinalDialogFinished(bb::system::SystemUiResult::Type);
    private:
    void showFinalDialog();

    virtual ~cBBNetworkManagerChild();
};

#endif /* CBBNETWORKMANAGERCHILD_H_ */
