#include "StatusEvent.h"
#include <bps/bps.h>
#include <bps/netstatus.h>

StatusEvent::StatusEvent ()
{
    subscribe(netstatus_get_domain());

    bps_initialize();

    // Request all network status events.
    netstatus_request_events(0);

    info = NULL;
}

StatusEvent::~StatusEvent ()
{
    bps_shutdown();
}

void StatusEvent::event ( bps_event_t *event )
{
    bool status = false;
    const char* interface = "";
    const char* type = "Unknown";
    netstatus_interface_details_t* details = NULL;

    // Verify that the event coming in is a network status event.
    if (bps_event_get_domain(event) == netstatus_get_domain())
    {
        // Using the BPS event code of the network status event,
        // verify that the event is a network information event.
        if (NETSTATUS_INFO == bps_event_get_code(event))
        {
            // Retrieve the network status information, and verify
            // that the procedure is successful.
            if (BPS_SUCCESS == netstatus_get_info(&info))
            {
                status = netstatus_info_get_availability(info);
                interface = netstatus_info_get_default_interface(info);
                int success = netstatus_get_interface_details(interface, &details);

                if (success == BPS_SUCCESS)
                {
                    switch (netstatus_interface_get_type(details))
                    {
                        case NETSTATUS_INTERFACE_TYPE_WIRED:
                            type = "Wired";
                            break;

                        case NETSTATUS_INTERFACE_TYPE_WIFI:
                            type = "Wifi";
                            break;

                        case NETSTATUS_INTERFACE_TYPE_BLUETOOTH_DUN:
                            type = "Bluetooth";
                            break;

                        case NETSTATUS_INTERFACE_TYPE_USB:
                        case NETSTATUS_INTERFACE_TYPE_BB:
                            type = "Usb";
                            break;

                        case NETSTATUS_INTERFACE_TYPE_VPN:
                            type = "Vpn";
                            break;


                        case NETSTATUS_INTERFACE_TYPE_CELLULAR:
                            type = "Cellular";
                            break;

                        case NETSTATUS_INTERFACE_TYPE_P2P:
                        case NETSTATUS_INTERFACE_TYPE_UNKNOWN:
                            type = "Unknown";
                            break;
                    }
                    netstatus_free_info(&info);
                }
            }

            // Emit the signal to trigger networkStatusUpdated slot.
            emit networkStatusUpdated(status, type);
        }
    }
}
