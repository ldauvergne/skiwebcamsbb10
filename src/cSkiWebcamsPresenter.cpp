/*
 * cSkiWebcamsPresenter.cpp
 *
 *  Created on: 7 déc. 2013
 *      Author: Pipould
 */

#include "cSkiWebcamsPresenter.hpp"


using namespace std;
using namespace bb::cascades;

cSkiWebcamsPresenter::cSkiWebcamsPresenter(QObject *parent) :
        QObject(parent)
{
    countries_names = new GroupDataModel(this);
    resorts_names = new GroupDataModel(this);
    favorites_names = new GroupDataModel(this);
    webcams_url = new GroupDataModel(this);

    aCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(aCodec);
    QTextCodec::setCodecForLocale(aCodec);
    QTextCodec::setCodecForCStrings(aCodec);

    // Disable grouping in data model
    countries_names->setGrouping(ItemGrouping::None);
    resorts_names->setGrouping(ItemGrouping::None);
    favorites_names->setGrouping(ItemGrouping::None);
    webcams_url->setGrouping(ItemGrouping::None);

    mSetCountriesViewMessage(new QString("Loading ..."));

    aDataDate = new QString("");
    aDataHandler = NULL;

    aSelectedCountry = 0;
}

void cSkiWebcamsPresenter::mInitPresenter()
{
    qDebug() << "Entering mInitPresenter";
    aDataHandler = new cSkiWebcamsDataHandler(FILE_TO_LOAD,FAVS_TO_LOAD); //"app/native/assets/StationsFinal35.xml");//
    aDataDate = new QString(QString::fromUtf8(aDataHandler->mGetDate()));
    emit sDataDateChanged();
    qDebug() << "Finished mInitPresenter";
}

void cSkiWebcamsPresenter::mLoadAll(){
    aDataHandler->mLoadAll();
}

void cSkiWebcamsPresenter::mGetCountriesName()
{
    // Clear the old account information from the model
    countries_names->clear();

    list<const char*> lCountries = aDataHandler->mGetCountriesName();

    for (int lIndex = aDataHandler->mGetCountriesName().size() - 1; lIndex >= 0; lIndex--) {
        QVariantMap country;

        list<const char*>::iterator i = lCountries.begin();
        std::advance(i, lIndex);

        country["countryName"] = aCodec->toUnicode(*i);

        // Add the entry to the model
        countries_names->insert(country);
    }
}

void cSkiWebcamsPresenter::mGetResortsName(int pCountryIndex)
{
    // Clear the old account information from the model
    resorts_names->clear();
    list<const char*> lResorts = aDataHandler->mGetResortsName(pCountryIndex);
    for (int lIndex = lResorts.size() - 1; lIndex >= 0;
            lIndex--) {
        QVariantMap resort;

        list<const char*>::iterator i = lResorts.begin();
        std::advance(i, lIndex);

        resort["resortName"] = aCodec->toUnicode(*i);

        // Add the entry to the model
        resorts_names->insert(resort);
    }
}

void cSkiWebcamsPresenter::mGetFavoritesName()
{
    // Clear the old account information from the model
    favorites_names->clear();

    list<const char*> lFavorites = aDataHandler->mGetFavoritesName();
    for (int lIndex = lFavorites.size() - 1; lIndex >= 0;
            lIndex--) {
        QVariantMap favorite;

        list<const char*>::iterator i = lFavorites.begin();
        std::advance(i, lIndex);

        favorite["resortName"] = aCodec->toUnicode(*i);

        // Add the entry to the model
        favorites_names->insert(favorite);
    }
    if(lFavorites.size()==0){
        aHasFavourites=false;
    }
    else{
        aHasFavourites=true;
    }
    emit sHasFavouritesChanged();
}

void cSkiWebcamsPresenter::mAddFavorite(int pResortIndex)
{
    qDebug() << "Entering mAddFavorite";
    aDataHandler->mAddFavorite(aSelectedCountry,pResortIndex);
    mGetFavoritesName();
}

void cSkiWebcamsPresenter::mDeleteFavorite(int pIndex)
{
    qDebug() << "Entering mDeleteFavorite";
    aDataHandler->mDeleteFavorite(pIndex);
    mGetFavoritesName();
}

void cSkiWebcamsPresenter::mGetFavoriteWebcams(int pIndex)
{
    webcams_url->clear();
    foreach (const char* webcam_url, aDataHandler->mGetFavoriteUrl(pIndex))
    {
        QVariantMap webcam;
        QString* url = new QString(aCodec->toUnicode(webcam_url));
        webcam["webcam_url"] = *url;
        // Add the entry to the model
        webcams_url->insert(webcam);
    }
}

void cSkiWebcamsPresenter::mGetWebcams(int pCountryIndex,int pResortIndex)
{
    // Clear the old account information from the model
    webcams_url->clear();
    foreach (const char* webcam_url, aDataHandler->mGetWebcams(pCountryIndex,pResortIndex))
    {

        QVariantMap webcam;
        QString* url = new QString(aCodec->toUnicode(webcam_url));
        webcam["webcam_url"] = *url;
        // Add the entry to the model
        webcams_url->insert(webcam);
    }
}

void cSkiWebcamsPresenter::mGetWebcams(int pResortIndex)
{
    // Clear the old account information from the model
    webcams_url->clear();
    foreach (const char* webcam_url, aDataHandler->mGetWebcams(aSelectedCountry,pResortIndex))
    {
        QVariantMap webcam;
        QString* url = new QString(aCodec->toUnicode(webcam_url));
        webcam["webcam_url"] = *url;
        // Add the entry to the model
        webcams_url->insert(webcam);
    }
}

void cSkiWebcamsPresenter::mSetSelectedCountry(int pCountryIndex)
{
    aSelectedCountry = pCountryIndex;
}

void cSkiWebcamsPresenter::mSetCountriesViewMessage(QString* pMessage)
{
    aCountriesViewMessage = pMessage;
    emit sCountryMessageChanged();
}

GroupDataModel* cSkiWebcamsPresenter::countries_model() const
{
    return countries_names;
}

GroupDataModel* cSkiWebcamsPresenter::resorts_model() const
{
    return resorts_names;
}

GroupDataModel* cSkiWebcamsPresenter::webcams_url_model() const
{
    return webcams_url;
}

GroupDataModel* cSkiWebcamsPresenter::favorites_model() const
{
    return favorites_names;
}

QString cSkiWebcamsPresenter::getCountriesViewMessage()
{
    return *aCountriesViewMessage;
}

QString cSkiWebcamsPresenter::mGetDataDate()
{
    return *aDataDate;
}

bool cSkiWebcamsPresenter::mHasFavourites()
{
    return aHasFavourites;
}

cSkiWebcamsPresenter::~cSkiWebcamsPresenter()
{
    // TODO Auto-generated destructor stub
}

