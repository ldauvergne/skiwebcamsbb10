import bb.cascades 1.2
import org.labsquare 1.0

Container {
    id: webcamView
    property variant webImageViewurl

    layout: DockLayout {
    }

    ActivityIndicator {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        preferredHeight: 150
        visible: true
        running: true
    }

    WebImageView {
        id: webViewImage
        url: webImageViewurl
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        visible: (webViewImage.loading == 1.0)
    }

}