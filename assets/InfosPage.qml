import bb.cascades 1.2

Page {
    signal done()
    titleBar: TitleBar {
        title: qsTr("About")
        dismissAction: ActionItem {
            title: qsTr("Close")
            onTriggered: {
                done()
            }
        }
    }
        

    Container {

        layout: StackLayout {
        }
        
        Label {
            text: "Ski Webcams" + Retranslate.onLocaleOrLanguageChanged
            horizontalAlignment: HorizontalAlignment.Center
            
            textStyle {
                base: SystemDefaults.TextStyles.TitleText
            }
        }
        Label {
            text: oSkiWebcamsPresenter.aDataDate + Retranslate.onLocaleOrLanguageChanged
            horizontalAlignment: HorizontalAlignment.Center
            
            textStyle {
                base: SystemDefaults.TextStyles.SubtitleText
            }
        }
        
        ScrollView {
            id: scrollView
            accessibility.name: "scrollView"
            
            WebView {
                preferredHeight: DisplayInfo.height
                accessibility.name: "viewHelp"

                url: qsTr("local:///assets/pages/about.html")

                onNavigationRequested: {
                    if (request.navigationType != WebNavigationType.Other) {
                        request.action = WebNavigationRequestAction.Ignore;

                        invokeWebBrowser(request.url);

                    } else {
                        request.action = WebNavigationRequestAction.Accept;
                    }
                }

                function invokeWebBrowser(urlPage) {
                    linkInvocation.query.uri = urlPage;
                }

                attachedObjects: [
                    Invocation {
                        id: linkInvocation

                        query {
                            onUriChanged: {
                                linkInvocation.query.updateQuery();
                            }
                        }

                        onArmed: {
                            trigger("bb.action.OPEN");
                        }
                    }
                ]
            }
        }
    }
}
