import bb.cascades 1.2

NavigationPane {
    id: nav
    Page {
        titleBar: TitleBar {

            // Localized text with the dynamic translation and locale updates support
            title: qsTr("World Page") + Retranslate.onLocaleOrLanguageChanged
        }

        attachedObjects: [
            // Definition of the second Page, used to dynamically create the Page above.

            ComponentDefinition {
                id: countryPage
                source: "CountryPage.qml"
            },
            ComponentDefinition {
                id: webcamsPage
                source: "WebcamsPage.qml"
            },
            ComponentDefinition {
                id: webcamPage
                source: "WebcamPage.qml"
            },
            ComponentDefinition {
                id: mailPage
                source: "MailPage.qml"
            }

        ]

        Container {
            
            layout: DockLayout {
            }
            
            Label {
                id: errorlabel
                text: oSkiWebcamsPresenter.aCountriesViewMessage + Retranslate.onLocaleOrLanguageChanged
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center

                textStyle {
                    base: SystemDefaults.TextStyles.TitleText
                    color: Color.Gray
                }
                visible: oSkiWebcamsPresenter.countries_model.isEmpty()
            }
            
            ListView {
                dataModel: oSkiWebcamsPresenter.countries_model

                listItemComponents: ListItemComponent {
                    type: "item"

                    StandardListItem {
                        id: countryitem
                        title: ListItemData.countryName
                        onCreationCompleted: {
                            countryitem.ListItem.view.mSetCountriesTextVisible()
                        }
                    }

                }
                
                function mSetCountriesTextVisible() {
                    errorlabel.visible = oSkiWebcamsPresenter.countries_model.isEmpty()
                }
                
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    oSkiWebcamsPresenter.mSetSelectedCountry(indexPath)
                    oSkiWebcamsPresenter.mGetResortsName(indexPath)

                    nav.push(countryPage.createObject())

                }

            }

        }
    }

}
