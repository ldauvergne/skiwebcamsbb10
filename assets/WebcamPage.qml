import bb.cascades 1.2
import org.labsquare 1.0
Page {
    property variant lWebcamURL
    property variant lResortName
    
    titleBar: TitleBar {
        title: qsTr("Webcam - ") + lResortName + Retranslate.onLocaleOrLanguageChanged
    }
    ScrollView {
        id: scrollView

        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        Container {
            
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            layout: DockLayout {
            }

            WebcamView {
                id: lWebcamView
                webImageViewurl: lWebcamURL
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
            }

        }

        scrollViewProperties {
            scrollMode: ScrollMode.Both
            pinchToZoomEnabled: true
            minContentScale: 0.7
            maxContentScale: 3
        }
        
        contextActions: [
            ActionSet {
                title: qsTr("Navigation")
                
                ActionItem {
                    title: qsTr("Report issue")
                    imageSource: "asset:///images/ic_email_dk.png"
                    onTriggered: {
                        emailInvocation.query.uri = "mailto:leopold.dauvergne@yahoo.com?subject=Webcam URL Issue : "+ lResortName +"&body=" + lWebcamURL
                        emailInvocation.query.updateQuery();
                        
                        /*
                        var lMailPage = mailPage.createObject()
                        lMailPage.lWebcamURL = lWebcamURL
                        nav.push(lMailPage)*/
                    }
                }
            }
        ]
        
        attachedObjects: [
            Invocation {
                id: emailInvocation
                query.mimeType: "text/plain"
                query.invokeTargetId: "sys.pim.uib.email.hybridcomposer"
                query.invokeActionId: "bb.action.SENDEMAIL"
                onArmed: {
                    emailInvocation.trigger(emailInvocation.query.invokeActionId);
                }
            }
        ]
    }

}