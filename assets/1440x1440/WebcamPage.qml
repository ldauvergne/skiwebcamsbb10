import bb.cascades 1.2
import org.labsquare 1.0
Page {
    property variant lWebcamURL

    titleBar: TitleBar {
        title: qsTr("Webcam") + Retranslate.onLocaleOrLanguageChanged
    }
    ScrollView {
        id: scrollView

        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        Container {
            
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            layout: DockLayout {
            }

            WebcamView {
                id: lWebcamView
                webImageViewurl: lWebcamURL
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }

        }

        scrollViewProperties {
            scrollMode: ScrollMode.Both
            pinchToZoomEnabled: true
            minContentScale: 0.7
            maxContentScale: 3
        }
    }

}