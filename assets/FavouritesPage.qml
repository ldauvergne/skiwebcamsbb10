/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2
import bb.cascades 1.2

NavigationPane {
    id: nav
    Page {
        titleBar: TitleBar {

            // Localized text with the dynamic translation and locale updates support
            title: qsTr("Favourites") + Retranslate.onLocaleOrLanguageChanged
        }

        attachedObjects: [
            // Definition of the second Page, used to dynamically create the Page above.

            ComponentDefinition {
                id: webcamsPage
                source: "WebcamsPage.qml"
            },
            ComponentDefinition {
                id: webcamPage
                source: "WebcamPage.qml"
            }

        ]
        Container {

            layout: DockLayout {
            }

            Label {
                id: emptyfavouritelabel
                text:  qsTr("Favourites will show up here") + Retranslate.onLocaleOrLanguageChanged
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center

                textStyle {
                    base: SystemDefaults.TextStyles.TitleText
                    color: Color.Gray
                }
                visible: !oSkiWebcamsPresenter.aHasFavourites
            }

            ListView {
                dataModel: oSkiWebcamsPresenter.favorites_model

                listItemComponents: ListItemComponent {

                    type: "item"
                    StandardListItem {
                        id: favoriteitem
                        title: ListItemData.resortName
                        contextActions: [
                            ActionSet {
                                title: qsTr("Navigation")

                                ActionItem {
                                    title: qsTr("Delete favourite")
                                    imageSource: "asset:///images/icon_215.png"
                                    onTriggered: {
                                        favoriteitem.ListItem.view.mDeleteFav(favoriteitem.ListItem.indexPath)
                                    }
                                }
                            }
                        ]
                    }

                }
                
                function mDeleteFav(indexPath) {
                    oSkiWebcamsPresenter.mDeleteFavorite(indexPath);
                }

                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    oSkiWebcamsPresenter.mGetFavoriteWebcams(indexPath)
                    nav.push(webcamsPage.createObject());
                }
            }
        }

    }
}