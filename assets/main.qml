/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2

TabbedPane {
    id: navigationPane
    showTabsOnActionBar: true
    activeTab: tabWorld

    attachedObjects: [
        Sheet {
            id: infosPage
            InfosPage {
                onDone: {
                    infosPage.close()
                }
            }
        }
    ]
    
    // Add the application menu using a MenuDefinition
    Menu.definition: MenuDefinition {
        
        // Specify the actions that should be included in the menu
        actions: [
            ActionItem {
                title: "Info"
                
                imageSource: "asset:///images/ic_info.png"
                onTriggered: {
                    infosPage.open()
                }
                ActionBar.placement: ActionBarPlacement.OnBar
            }
        ] // end of actions list
    } // end of 
    
    Tab { //World tab
        id: tabWorld
        title: qsTr("World") + Retranslate.onLocaleOrLanguageChanged
        
        imageSource: "asset:///images/ic_map.png"
        
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateImmediately
        
        delegate: Delegate {
            source: "WorldPage.qml"
        }
    }
    
    Tab { //Favs tab
        id: tabFavs
        title: qsTr("Favourites") + Retranslate.onLocaleOrLanguageChanged
        
        imageSource: "asset:///images/icon_215.png"
        
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateImmediately
        
        delegate: Delegate {
            source: "FavouritesPage.qml"
        }
    
    }
}
