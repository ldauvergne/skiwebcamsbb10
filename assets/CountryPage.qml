/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2

Page {
    titleBar: TitleBar {
        title: qsTr("Countries") + Retranslate.onLocaleOrLanguageChanged
    }

    Container {

        ListView {
            dataModel: oSkiWebcamsPresenter.resorts_model

            listItemComponents: ListItemComponent {
                type: "item"
                StandardListItem {
                    id: countryitem
                    title: ListItemData.resortName

                    contextActions: [
                        ActionSet {
                            title: qsTr("Navigation")

                            ActionItem {
                                title: qsTr("Add favourite")
                                imageSource: "asset:///images/icon_215.png"
                                onTriggered: {
                                    countryitem.ListItem.view.mAddFav(countryitem.ListItem.indexPath)
                                }
                            }
                        }
                    ]
                }
            }
            function mAddFav(indexPath) {
                        oSkiWebcamsPresenter.mAddFavorite(indexPath);
            }

            onTriggered: {
                clearSelection()
                select(indexPath)
                oSkiWebcamsPresenter.mGetWebcams(indexPath)
                
                var lWebcamsPage = webcamsPage.createObject()
                lWebcamsPage.lResortName = oSkiWebcamsPresenter.resorts_model.data(indexPath).resortName
                nav.push(lWebcamsPage)
            }
        }

    }
}
