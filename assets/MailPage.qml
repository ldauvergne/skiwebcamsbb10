import bb.cascades 1.2

Page {
    property variant lWebcamURL

    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        layout: DockLayout {
        }

        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center

            TextArea {
                id: emailBody
            }

            Container {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                preferredHeight: 250
                
                Button {
                    text: "Send email"

                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center

                    onClicked: {
                        emailInvocation.query.uri = "mailto:leopold.dauvergne@yahoo.com?subject=Webcam URL Issue&body=" + lWebcamURL + emailBody.text
                        emailInvocation.query.updateQuery();
                    }
                }
            }
        }
    }

    attachedObjects: [
        Invocation {
            id: emailInvocation
            query.mimeType: "text/plain"
            query.invokeTargetId: "sys.pim.uib.email.hybridcomposer"
            query.invokeActionId: "bb.action.SENDEMAIL"
            onArmed: {
                emailInvocation.trigger(emailInvocation.query.invokeActionId);
            }
        }
    ]
}