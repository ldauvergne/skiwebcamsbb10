import bb.cascades 1.2

Page {
    property variant lResortName
    
    titleBar: TitleBar {
        title: qsTr("Webcams - ") + lResortName + Retranslate.onLocaleOrLanguageChanged
    }

    Container {

        ListView {
            id: webcamsView
            layout: GridListLayout {
                columnCount: 1
                cellAspectRatio: 1.6
            }
            dataModel: oSkiWebcamsPresenter.webcams_url_model

            onTriggered: {
                clearSelection()
                select(indexPath)
                var lWebcamPage = webcamPage.createObject()
                lWebcamPage.lWebcamURL = oSkiWebcamsPresenter.webcams_url_model.data(indexPath).webcam_url
                lWebcamPage.lResortName = lResortName
                nav.push(lWebcamPage)
            }

            listItemComponents: ListItemComponent {
                type: "item"
                WebcamView {
                    webImageViewurl: ListItemData.webcam_url
                }

            }
        }
    }
}
