<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>CountryPage</name>
    <message>
        <location filename="../assets/CountryPage.qml" line="21"/>
        <source>Countries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CountryPage.qml" line="37"/>
        <source>Navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CountryPage.qml" line="40"/>
        <source>Add favourite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FavouritesPage</name>
    <message>
        <location filename="../assets/FavouritesPage.qml" line="26"/>
        <source>Favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/FavouritesPage.qml" line="49"/>
        <source>Favourites will show up here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/FavouritesPage.qml" line="71"/>
        <source>Navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/FavouritesPage.qml" line="74"/>
        <source>Delete favourite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfosPage</name>
    <message>
        <location filename="../assets/InfosPage.qml" line="6"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/InfosPage.qml" line="8"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/InfosPage.qml" line="46"/>
        <source>local:///assets/pages/about.html</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebcamPage</name>
    <message>
        <location filename="../assets/1440x1440/WebcamPage.qml" line="7"/>
        <location filename="../assets/720x720/WebcamPage.qml" line="7"/>
        <source>Webcam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/WebcamPage.qml" line="8"/>
        <source>Webcam - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/WebcamPage.qml" line="42"/>
        <source>Navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/WebcamPage.qml" line="45"/>
        <source>Report issue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebcamsPage</name>
    <message>
        <location filename="../assets/WebcamsPage.qml" line="7"/>
        <source>Webcams - </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorldPage</name>
    <message>
        <location filename="../assets/WorldPage.qml" line="9"/>
        <source>World Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="54"/>
        <source>World</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="67"/>
        <source>Favourites</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
